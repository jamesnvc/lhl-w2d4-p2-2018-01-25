//
//  OtherViewController.m
//  KVODemo
//
//  Created by James Cash on 25-01-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "OtherViewController.h"

@interface OtherViewController ()

@property (nonatomic,weak) IBOutlet UITextField *textField;

@end

@implementation OtherViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
